import React from "react";

export default function Display({ data }) {
  return (
    <div>
      <ul>
        {data.map((item, index) => (
          <li key={item._id}>
            Tutorail {index + 1} = <a>{item.title}</a>
          </li>
        ))}
      </ul>
    </div>
  );
}
