import './App.css';
import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import { fetchArticle, postTuturail } from './redux/actions/tutorialsAction';
import { bindActionCreators } from 'redux';
import Display from './components/Display';

function App() {

  const [tuturail, setTuturail] = useState([{ _id: "12",  title: "Exam",   description: "Sad" }])

  const dispatch = useDispatch()
  const state = useSelector(state => state.articles)

  const onPost = bindActionCreators(postTuturail(tuturail), dispatch);


  useEffect(() => {
    dispatch(fetchArticle())
  }, [])

  return (
    <Container>

     {console.log(state)}
     <button onClick={()=>console.log(state)}>Get Data</button>
     <button onClick={()=>onPost}>Post</button>
    <Display data={state}/>

    </Container>
  );
}

export default App;
