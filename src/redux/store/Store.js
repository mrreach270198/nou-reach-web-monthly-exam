import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { tutorialsReducer } from "../reducers/tutorialsReducer";

export const store = createStore(tutorialsReducer, applyMiddleware(thunk, logger))