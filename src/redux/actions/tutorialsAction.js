import api from "../../utils/api"

export const fetchArticle = () => async dp =>{

    let response = await api.get('/tutorials');

    return dp({
        type: "FETCH",
        payload: response.data.data
    })
}

export const postTuturail = (data) => async dp =>{

    let response = await api.post('/tutorials',+data);

    return dp({
        type: "POST",
        payload: response.data.data
    })
}