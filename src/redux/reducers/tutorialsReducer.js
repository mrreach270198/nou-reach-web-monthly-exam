const initialState = {
    tutorials: [],
}

export const tutorialsReducer = (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH":
        return { ...state, tutorials: payload }
    
        case "POST":
        return { ...state,  payload}

    default:
        return state
    }
}